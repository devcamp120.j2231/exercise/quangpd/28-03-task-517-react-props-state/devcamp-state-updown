import { Component } from "react"

class CountClickUpDown extends Component{
    constructor(props){
        super(props);

        this.state ={
            number : 0
        }
    }

    buttonUpNumber =() =>{
        this.setState({
            number : this.state.number + 1
        })
    }

    buttonDownNumber =() =>{
        this.setState({
            number : this.state.number - 1
        })
    }
    render(){
        return(
            <>
                <div>
                    <div className="row text-center">
                        <div className="col-sm-12">
                            <h4>Hãy Tăng Hoặc Giảm Số ! </h4>
                        </div>
                    </div>
                    <div className="text-center">
                        <button onClick={this.buttonUpNumber} className="btn btn-danger text-center mt-3">Tăng</button>
                        <br/>
                        <p className="mt-3">Number : {this.state.number}</p>
                        <button onClick={this.buttonDownNumber} className="btn btn-primary">Giảm</button>
                    </div>
                </div>
            </>
        )
    }
}

export default CountClickUpDown